### 1.sitesCMS简介
sitesCMS 是基于 **JFinal** 的 **多站点** CMS内容管理系统，遵循JFinal极简设计理念，轻量级、易扩展、学习简单，除JFinal外无其他重度依赖。精简的多站点功能设计，极易二次开发，一天一个网站不是梦。

[查看视频介绍及功能演示](https://ke.qq.com/course/3030309?taid=10143016244100389&tuin=92419b8c)

[详细使用说明请移步Wiki](https://gitee.com/xhhxb/sitesCMS/wikis/01.sitesCMS)

[更新计划请看这里](https://gitee.com/xhhxb/sitesCMS/wikis/08.%E6%9B%B4%E6%96%B0%E8%AE%A1%E5%88%92?sort_id=2987781)

### 2.内置功能
#### 2.1.功能说明
- 站点管理：多站点管理，可以在同一个程序包内同时实现多站点的开发、管理，主站点一键切换；
- 权限管理：使用JFinal独有的权限控制方式对系统功能进行管控，极简设计，无第三方依赖，支持菜单、按钮两级控制；
- 角色管理：角色权限分配，权限的实际拥有者，用户通过分配角色获取系统权限；
- 用户管理：后台管理用户，支持增删改查管理、角色分配；
- 栏目管理：文章的栏目分类，支持父子两级栏目；
- 文章管理：文章分为普通图文、图集、视频三类，支持置顶、缩略图、附件等功能；
- 日志管理：分为访问日志和管理日志，访问日志用于记录前端用户访问情况，管理日志记录后台用户管理操作，两者皆可单独设定是否开启；

#### 2.2.功能预览
![后台登录界面](https://images.gitee.com/uploads/images/2020/1004/173816_4c3b25b0_1008099.png "登录-五摩西官网.png")![文章管理](https://images.gitee.com/uploads/images/2020/1004/173841_b867e137_1008099.png "文章管理.png")
![用户管理](https://images.gitee.com/uploads/images/2020/1004/173903_6523dcdb_1008099.png "用户管理.png")![角色管理](https://images.gitee.com/uploads/images/2020/1004/173915_59de2623_1008099.png "角色管理.png")
![权限管理](https://images.gitee.com/uploads/images/2020/1004/173929_8f479556_1008099.png "权限管理.png")![日志管理](https://images.gitee.com/uploads/images/2020/1004/173940_b410a2ab_1008099.png "日志管理.png")

### 3.技术选型
#### 3.1.系统环境
- jdk 1.8
- MySQL 5.0.96
- maven 3.6.3

#### 3.2.主框架
- JFinal4.9全家桶（JFinal+Enjoy+ActiveRecord+JFinal-Undertow）

#### 3.3.持久层
- Alibaba Druid 1.0.29

#### 3.4.视图层
- 管理端：Layui 2.5.6
- 访问端：任何一个你喜欢的UI框架，可以是Bootstrap、可以是Layui、可以是pintuer、也可以什么都不是，这也是多站点管理的精髓， **彼此独立** 。

### 4.互动交流
#### 4.1.QQ群（1134290422）
![QQ群](https://images.gitee.com/uploads/images/2020/1003/163409_5b26cb97_1008099.png "sitesCMS3.png")

#### 4.2.公众号（sitesCMS）
![微信公众号](https://images.gitee.com/uploads/images/2020/0906/214509_490cbea7_1008099.jpeg "qrcode_for_gh_fbd4bda6778f_430.jpg")