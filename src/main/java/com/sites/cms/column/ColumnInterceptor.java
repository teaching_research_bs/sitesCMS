package com.sites.cms.column;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.sites.common.SiteInfo;

/**
 * 栏目拦截器(主要是针对controller中部分方法的参数校验，可考虑使用validator替代)
 * 
 * @author zyg
 * 2020年2月3日 上午11:40:09
 */
public class ColumnInterceptor implements Interceptor {

	public void intercept(Invocation inv) {
		Ret ret = Ret.fail();
		Controller controller = inv.getController();
		String methodName = inv.getMethodName();
		
		if("saveColumn".equals(methodName) || "updateColumn".equals(methodName)){
			String name = controller.getPara("name");
			String enName = controller.getPara("enName");
			if(StrKit.isBlank(name) || StrKit.isBlank(enName)){
				ret.set(SiteInfo.msgKey, SiteInfo.paraMiss);
				controller.renderJson(ret);
				return;
			}
			
			String regexZS = "[0-9A-Za-z]+";
			if(!enName.matches(regexZS)){
				ret.set(SiteInfo.msgKey, "英文标识只能是字母和数字！");
				controller.renderJson(ret);
				return;
			}
		}
		
		inv.invoke();
	}

}
