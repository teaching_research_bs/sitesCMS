package com.sites.cms.index;

import com.jfinal.aop.Inject;
import com.jfinal.kit.Ret;
import com.sites.cms.accessLog.AccessLogService;
import com.sites.cms.account.AccountService;
import com.sites.cms.article.ArticleService;

/**
 * cms管理端主页服务层
 * 
 * @author zyg
 * 2020年8月4日 下午10:48:34
 */
public class IndexService {

	@Inject(AccessLogService.class)
	AccessLogService accessLogService;
	
	@Inject(ArticleService.class)
	ArticleService articleService;
	
	@Inject(AccountService.class)
	AccountService accountService;
	
	/**
	 * 获取cms首页中需要展示的系统数据
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Ret getIndexData(){
		Ret ret = Ret.ok();
		
		// 获取首页访问数
		int indexAccess = accessLogService.countIndexAccess();
		ret.set("indexAccess", indexAccess);
		
		// 获取文章总数
		ret.putAll(articleService.queryArticleNums());
		
		// 获取文章点击数
		int artClickNums = articleService.countArtClickNums();
		ret.set("artClickNums", artClickNums);
		
		// 获取用户数
		int accountNums = accountService.queryAllAccounts().size();
		ret.set("accountNums", accountNums);
		
		return ret;
	}
	
}
