package com.sites.cms.index;

import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.sites.cms.permission.Remark;

/**
 * cms管理端主页控制器
 * 
 * @author zyg
 * 2020年1月4日 下午2:22:34
 */
public class IndexController extends Controller {

	@Inject
	IndexService srv;
	
	@Remark("菜单：后台首页")
	public void index(){
		Ret ret = srv.getIndexData();
		set("ret", ret);
		
		render("index.html");
	}
}
