package com.sites.cms.common;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.jfinal.log.Log4jLog;
import com.sites.common.SiteInfo;

/**
 * CMS全局异常处理
 * <br/>后续的controller和service中如无必要都没有进行异常处理，理论上所有的异常都会抛到这一层
 * <br/>能够根据controller中的render类型返回不同内容
 * 
 * @author zyg
 * 2020年1月30日 下午10:02:45
 */
public class GlobalErrorInterceptor implements Interceptor {

	private Log4jLog log = Log4jLog.getLog(GlobalErrorInterceptor.class);
	
	public void intercept(Invocation inv) {
		try {
			inv.invoke();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("抓获全局异常", e);
			/*
			 * 后台抛出异常时controller中并没有成功调用render方法， 所以这时是获取不到render类型的
			 * 但是在设计时方法名字是有规则的，凡是进入页面的都是enterXXX的方法名，其他的都是返回json的
			 * 暂时可以根据方法名来处理不同的异常情况
			 */
			Controller controller = inv.getController();
			String methodName = inv.getMethodName();
			if(methodName.startsWith("enter")){
				controller.render("/cms/common/500.html");
			} else {
				Ret ret = Ret.fail(SiteInfo.msgKey, "服务响应异常，请联系管理员处理！");
				controller.renderJson(ret);
			}
		}
	}

}
