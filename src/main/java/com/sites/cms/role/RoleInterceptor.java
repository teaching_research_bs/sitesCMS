package com.sites.cms.role;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.sites.common.SiteInfo;

/**
 * RoleController拦截器
 * <br/>主要用于部分方法的参数校验
 * 
 * @author zyg
 * 2020年1月31日 下午1:11:42
 */
public class RoleInterceptor implements Interceptor {

	public void intercept(Invocation inv) {
		Ret ret = Ret.fail();
		Controller c = inv.getController();
		String methodName = inv.getMethodName();

		/**
		 * 保存角色&更新角色校验
		 */
		if("saveRole".equals(methodName)||"updateRole".equals(methodName)){
			String name = c.getPara("name");
			if(StrKit.isBlank(name)){
				ret.set(SiteInfo.msgKey, SiteInfo.paraMiss);
				c.renderJson(ret);
				return;
			}
		}
		
		inv.invoke();
	}

}
