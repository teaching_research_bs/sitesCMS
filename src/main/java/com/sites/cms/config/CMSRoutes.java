package com.sites.cms.config;

import com.jfinal.config.Routes;
import com.sites.cms.accessLog.AccessLogController;
import com.sites.cms.account.AccountController;
import com.sites.cms.article.ArticleController;
import com.sites.cms.column.ColumnController;
import com.sites.cms.common.CmsAccessInterceptor;
import com.sites.cms.common.GlobalErrorInterceptor;
import com.sites.cms.common.pjax.PjaxInterceptor;
import com.sites.cms.index.IndexController;
import com.sites.cms.login.LoggedInterceptor;
import com.sites.cms.login.LoginController;
import com.sites.cms.permission.PermissionController;
import com.sites.cms.permission.PermissionInterceptor;
import com.sites.cms.role.RoleController;
import com.sites.cms.site.SiteController;

/**
 * cms路由配置
 * 
 * @author zyg
 * 2020年1月4日 下午1:15:19
 */
public class CMSRoutes extends Routes {

	@Override
	public void config() {
		setBaseViewPath("/cms");
		
		addInterceptor(new GlobalErrorInterceptor());//全局异常处理拦截器
		addInterceptor(new PjaxInterceptor());//pjax处理拦截器
		addInterceptor(new LoggedInterceptor());//登录限定拦截器
		addInterceptor(new PermissionInterceptor());//权限拦截器
		addInterceptor(new CmsAccessInterceptor());//访问信息拦截器
		
		add("/cmsLogin", LoginController.class, "login");
		add("/cms", IndexController.class, "index");
		add("/cmsAccount", AccountController.class, "account");
		add("/cmsRole", RoleController.class, "role");
		add("/cmsPerm", PermissionController.class, "permission");
		add("/cmsSite", SiteController.class, "site");
		add("/cmsColumn", ColumnController.class, "column");
		add("/cmsArt", ArticleController.class, "article");
		add("/cmsLog", AccessLogController.class, "accessLog");
	}

}
