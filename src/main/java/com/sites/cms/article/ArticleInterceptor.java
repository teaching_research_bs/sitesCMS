package com.sites.cms.article;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.sites.common.SiteInfo;

/**
 * 文章拦截器，主要用于校验文章信息
 * 
 * @author zyg
 * 2020年9月5日 下午4:15:18
 */
public class ArticleInterceptor implements Interceptor {

	public void intercept(Invocation inv) {
		Ret ret = Ret.fail();
		Controller controller = inv.getController();
		String methodName = inv.getMethodName();

		if("saveArt".equals(methodName)){
			String title = controller.getPara("title");
			if(StrKit.isBlank(title)){
				ret.set(SiteInfo.msgKey, "文章标题不可为空！");
				controller.renderJson(ret);
				return;
			}
			/*
			 * 如果需要校验标题长度等内容可以在这里编写对应逻辑
			 * 即便现在没有校验，前台也有会有好的异常提示，因为使用了全局的异常处理拦截器GlobalErrorInterceptor
			 */
			int column = controller.getParaToInt("column", 0);
			if(column <= 0){
				ret.set(SiteInfo.msgKey, "请选择栏目！");
				controller.renderJson(ret);
				return;
			}
			String content = controller.getPara("content");
			if(StrKit.isBlank(content)){
				ret.set(SiteInfo.msgKey, "文章内容不可为空！");
				controller.renderJson(ret);
				return;
			}
		}
		
		inv.invoke();
	}

}
