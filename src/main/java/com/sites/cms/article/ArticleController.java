package com.sites.cms.article;

import java.util.List;
import java.util.Map;

import com.jfinal.aop.Before;
import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Page;
import com.sites.cms.column.ColumnService;
import com.sites.cms.permission.Remark;
import com.sites.common.SiteInfo;
import com.sites.common.file.FileService;
import com.sites.common.model.Account;
import com.sites.common.model.Article;
import com.sites.common.model.Column;
import com.sites.common.model.Files;

/**
 * 文章控制器
 * 
 * @author zyg
 * 2020年9月5日 下午4:14:12
 */
@Before(ArticleInterceptor.class)
public class ArticleController extends Controller {

	@Inject
	private ArticleService srv;
	@Inject
	private ColumnService columnSrv;
	@Inject
	private FileService fileSrv;
	
	@Remark("菜单：新增文章")
	public void enterArtAdd(){
		Map<String, List<Column>> artColumnsMap = columnSrv.getArtColumns();
		setAttr("artColumnsMap", artColumnsMap);
		render("pageArtAdd.html");
	}
	
	@Remark("按钮：新增文章")
	public void saveArt(){
		Article article = getModel(Article.class, "");
		String[] files = getParaValues("files");
		Ret ret = srv.saveArt(article, files, (Account)getSessionAttr(SiteInfo.account));
		renderJson(ret);
	}
	
	@Remark("菜单：管理文章")
	public void enterArtMng(){
		int page = getParaToInt("page", 1);
		int limit = getParaToInt("limit", 8);
		String title = getPara("title", "");
		int column = getParaToInt("column", -1);
		String type = getPara("type", "0");
		keepPara();//保存参数
		Page<Article> artPage = srv.queryArtsPage(page, limit, title, column, type, SiteInfo.statusNormal);
		setAttr("artPage", artPage);
		Map<String, List<Column>> artColumnsMap = columnSrv.getArtColumns();
		setAttr("artColumnsMap", artColumnsMap);
		render("pageArtMng.html");
	}
	
	@Remark("菜单：文章回收站")
	public void enterArtDustbin(){
		int page = getParaToInt("page", 1);
		int limit = getParaToInt("limit", 8);
		String title = getPara("title", "");
		int column = getParaToInt("column", -1);
		String type = getPara("type", "0");
		keepPara();//保存参数
		Page<Article> artPage = srv.queryArtsPage(page, limit, title, column, type, SiteInfo.statusDel);
		setAttr("artPage", artPage);
		Map<String, List<Column>> artColumnsMap = columnSrv.getArtColumns();
		setAttr("artColumnsMap", artColumnsMap);
		render("pageArtDust.html");
	}
	
	@Remark("按钮：编辑文章")
	public void enterArtEdit(){
		int id = getParaToInt(0);
		String type = getPara(1);
		Article article = srv.getById(id);
		setAttr("art", article);
		Files thumbnail = fileSrv.getById(article.getThumbnail());
		setAttr("thb", thumbnail);
		List<Files> files = fileSrv.getByIds(article.getFiles());
		setAttr("files", files);
		Map<String, List<Column>> artColumnsMap = columnSrv.getArtColumns();
		setAttr("artColumnsMap", artColumnsMap);
		String page = "pageArtEdit.html";
		if("2".equals(type)){
			page = "pagePicArtEdit.html";
		}
		if("3".equals(type)){
			page = "pageVideoArtEdit.html";
		}
		render(page);
	}
	
	@Remark("按钮：更新文章")
	public void updateArt(){
		Article article = getModel(Article.class, "");
		String[] files = getParaValues("files");
		Ret ret = srv.updateArt(article, files, (Account)getSessionAttr(SiteInfo.account));
		renderJson(ret);
	}
	
	@Remark("按钮：删除文章")
	public void deleteArt(){
		int id = getInt(0);
		Ret ret = srv.deleteArt(id, (Account)getSessionAttr(SiteInfo.account));
		renderJson(ret);
	}
	
	@Remark("按钮：回收站中的发布文章")
	public void publishArt(){
		int id = getInt(0);
		Ret ret = srv.publishArt(id, (Account)getSessionAttr(SiteInfo.account));
		renderJson(ret);
	}
	
	@Remark("菜单：新增图文")
	public void enterPicArtAdd(){
		Map<String, List<Column>> artColumnsMap = columnSrv.getArtColumns();
		setAttr("artColumnsMap", artColumnsMap);
		render("pagePicArtAdd.html");
	}
	
	@Remark("按钮：保存图文")
	public void savePicArt(){
		Article article = getModel(Article.class, "");
		String[] files = getParaValues("files");
		Ret ret = srv.saveArt(article, files, (Account)getSessionAttr(SiteInfo.account));
		renderJson(ret);
	}
	
	@Remark("菜单：新增视频")
	public void enterVideoArtAdd(){
		Map<String, List<Column>> artColumnsMap = columnSrv.getArtColumns();
		setAttr("artColumnsMap", artColumnsMap);
		render("pageVideoArtAdd.html");
	}
	
	@Remark("按钮：保存视频")
	public void saveVideoArt(){
		Article article = getModel(Article.class, "");
		String[] files = getParaValues("files");
		Ret ret = srv.saveArt(article, files, (Account)getSessionAttr(SiteInfo.account));
		renderJson(ret);
	}
	
}
