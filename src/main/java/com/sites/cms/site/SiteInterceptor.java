package com.sites.cms.site;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.sites.common.SiteInfo;

/**
 * 站点管理拦截器(主要是校验controller中部分方法的参数)
 * 
 * @author zyg
 * 2020年2月1日 下午1:24:17
 */
public class SiteInterceptor implements Interceptor {

	public void intercept(Invocation inv) {
		Ret ret = Ret.fail();
		
		Controller controller = inv.getController();
		String methodName = inv.getMethodName();
		
		if("saveSite".equals(methodName) || "updateSite".equals(methodName)){
			String siteName = controller.getPara("siteName");
			String siteSign = controller.getPara("siteSign");
			if(StrKit.isBlank(siteName) || StrKit.isBlank(siteSign)){
				ret.set(SiteInfo.msgKey, SiteInfo.paraMiss);
				controller.renderJson(ret);
				return;
			}
			String regexZS = "[0-9A-Za-z]+";
			if(!siteSign.matches(regexZS)){
				ret.set(SiteInfo.msgKey, "站点标识只能是字母和数字！");
				controller.renderJson(ret);
				return;
			}
		}
		
		inv.invoke();
	}

}
