package com.sites.cms.account;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.sites.common.SiteInfo;

/**
 * AccountController拦截器
 * <br/>主要用于部分方法中的参数校验
 * 
 * @author zyg
 * 2020年1月31日 下午12:13:16
 */
public class AccountInterceptor implements Interceptor {

	public void intercept(Invocation inv) {
		Ret ret = Ret.fail();
		Controller c = inv.getController();
		String methodName = inv.getMethodName();
		
		/**
		 * 保存用户方法校验
		 */
		if("saveAccount".equals(methodName)){
			String nickName = c.getPara("nickName");
			String userName = c.getPara("userName");
			String password = c.getPara("password");
			if(StrKit.isBlank(nickName)||StrKit.isBlank(userName)||StrKit.isBlank(password)){
				ret.set(SiteInfo.msgKey, SiteInfo.paraMiss);
				c.renderJson(ret);
				return;
			}
		}
		
		/**
		 * 保存用户方法校验
		 */
		if("updateAccount".equals(methodName)){
			String nickName = c.getPara("nickName");
			if(StrKit.isBlank(nickName)){
				ret.set(SiteInfo.msgKey, SiteInfo.paraMiss);
				c.renderJson(ret);
				return;
			}
		}
		
		
		inv.invoke();
	}

}
