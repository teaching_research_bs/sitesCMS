package com.sites.cms.login;

import javax.servlet.http.HttpSession;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.sites.common.SiteInfo;
import com.sites.common.model.Account;

/**
 * 已登录拦截器
 * <br/>用于限定用户必须登录才能进行的操作
 * 
 * @author zyg
 * 2020年1月31日 上午11:19:03
 */
public class LoggedInterceptor implements Interceptor {

	public void intercept(Invocation inv) {
		Controller controller = inv.getController();
		HttpSession session = controller.getSession();
		Account account = (Account) session.getAttribute(SiteInfo.account);
		if(account == null){
			controller.redirect("/cmsLogin");
		} else {
			inv.invoke();
		}
		
	}

}
