package com.sites.common.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseFiles<M extends BaseFiles<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Integer id) {
		set("id", id);
	}
	
	public java.lang.Integer getId() {
		return getInt("id");
	}

	/**
	 * 文件类型：annex附件，thumbnail缩略图，Album相册
	 */
	public void setType(java.lang.String type) {
		set("type", type);
	}
	
	/**
	 * 文件类型：annex附件，thumbnail缩略图，Album相册
	 */
	public java.lang.String getType() {
		return getStr("type");
	}

	/**
	 * 文件名
	 */
	public void setName(java.lang.String name) {
		set("name", name);
	}
	
	/**
	 * 文件名
	 */
	public java.lang.String getName() {
		return getStr("name");
	}

	/**
	 * 原始文件名
	 */
	public void setOriginalName(java.lang.String originalName) {
		set("originalName", originalName);
	}
	
	/**
	 * 原始文件名
	 */
	public java.lang.String getOriginalName() {
		return getStr("originalName");
	}

	/**
	 * 文件保存路径
	 */
	public void setPath(java.lang.String path) {
		set("path", path);
	}
	
	/**
	 * 文件保存路径
	 */
	public java.lang.String getPath() {
		return getStr("path");
	}

	public void setContentType(java.lang.String contentType) {
		set("contentType", contentType);
	}
	
	public java.lang.String getContentType() {
		return getStr("contentType");
	}

	/**
	 * 创建时间
	 */
	public void setCreateTime(java.util.Date createTime) {
		set("createTime", createTime);
	}
	
	/**
	 * 创建时间
	 */
	public java.util.Date getCreateTime() {
		return get("createTime");
	}

	/**
	 * 文件说明
	 */
	public void setRemark(java.lang.String remark) {
		set("remark", remark);
	}
	
	/**
	 * 文件说明
	 */
	public java.lang.String getRemark() {
		return getStr("remark");
	}

}
