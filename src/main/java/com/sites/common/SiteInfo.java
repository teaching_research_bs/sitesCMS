package com.sites.common;

/**
 * 站点信息
 * 所有的站点信息都静态变量存储一份
 * 
 * @author zyg
 * 2020年1月2日 下午10:19:28
 */
public class SiteInfo {

	public static int siteId = 0;
	public static String siteName = "sitesCMS";
	public static String siteSign = "_main";
	public static String siteUrl = "";
	public static String uploadPath = "G:\\upload\\";
	public static String siteDes = "";
	public static int fileMaxSize = 35;//上传文件最大限定，单位M
	public static String fileSuffix = "txt|doc|docx|xls|xlsx|pdf|zip|rar|7z|ppt|pptx|jpg|png|gif|bmp|jpeg|mp4";
	public static String cdsAccessLog = "1";
	public static String cmsAccessLog = "1";
	
	/**
	 * 前后端交互msg的key
	 */
	public static String msgKey = "msg";
	/**
	 * 在session中存放登录用户的key
	 */
	public static String account = "account";
	/**
	 * 草稿状态的字符串
	 */
	public static String statusDraft = "0";
	/**
	 * 正常状态的字符串
	 */
	public static String statusNormal = "1";
	/**
	 * 删除状态的字符
	 */
	public static String statusDel = "2";
	/**
	 * 用户权限缓存名
	 */
	public static String accPermCacheName = "accountPermCacheName";
	/**
	 * 栏目缓存名
	 */
	public static String columnsCacheName = "columnsCacheName";
	/**
	 * 权限表缓存名
	 */
	public static String permCacheName = "permCacheName";
	/**
	 * 参数缺失的提醒信息
	 */
	public static String paraMiss = "必填参数不可为空！";
	/**
	 * 服务响应异常信息
	 */
	public static String serverError = "服务响应异常，请联系系统管理员！";
	/**
	 * 超级管理员的用户名，该用户不受权限控制
	 */
	public static String adminName = "wumoxi";
	/**
	 * 程序版本号<br/>
	 * 版本号分为3个部分，对应信息如下：<br/>
	 * 第一部分：大版本号，整体架构优化、调整该部分增加<br/>
	 * 第二部分：中版本号，新增功能、架构优化等较大内容调整该部分增加<br/>
	 * 第三部分：小版本号，bug修复、功能优化等较小内容调整该部分增加
	 */
	public static String version = "1.3.4";
	/**
	 * 版本更新时间<br/>
	 * 每一次更新版本号都同步修改更新时间
	 */
	public static String updateDate = "2020-10-06";
	/**
	 * 日志类型-cds
	 */
	public static String cdsLog = "cds";
	/**
	 * 日志类型-cms
	 */
	public static String cmsLog = "cms";
	/**
	 * 参数值长度的最大长度，当超过该超度时截取后存储，用于访问信息记录功能
	 */
	public static int paraValuesMaxLength = 100;
}
