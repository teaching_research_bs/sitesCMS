package com.sites.common.file;

import java.io.File;

import com.jfinal.aop.Before;
import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.jfinal.upload.UploadFile;
import com.sites.common.SiteInfo;
import com.sites.common.model.Files;

/**
 * 文件控制器
 * 
 * @author zyg
 * 2020年2月5日 下午5:33:03
 */
@Before(FileInterceptor.class)
public class FileController extends Controller {

	@Inject
	private FileService srv;
	
	/**
	 * 上传文件
	 */
	public void upload(){
		UploadFile uploadFile = getFile("file");//前端使用layui上传，默认名是file
		String type = getPara("type");
		Ret ret = srv.upload(uploadFile, type);
		renderJson(ret);
	}
	
	/**
	 * 富文本编辑器使用的图片上传方法
	 */
	public void uploadImg4Editor(){
		UploadFile uploadFile = getFile("file");//前端自定义的参数名
		String type = getPara("type");
		Ret ret = srv.uploadImg4Editor(uploadFile, type);
		renderJson(ret);
	}
	
	/**
	 * Layui预览图片使用
	 */
	public void getImg(){
		int imgId = getParaToInt(0);
		Ret ret = srv.getImg(imgId);
		renderJson(ret);
	}
	
	/**
	 * 下载文件
	 */
	public void download(){
		int id = getParaToInt(0);
		Files files = srv.getById(id);
		File file = new File(SiteInfo.uploadPath+files.getPath());
		renderFile(file,files.getOriginalName());
	}
	
}
