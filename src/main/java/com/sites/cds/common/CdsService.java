package com.sites.cds.common;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.sites.common.SiteInfo;
import com.sites.common.model.Article;
import com.sites.common.model.Column;
import com.sites.common.model.Files;

/**
 * cds公用服务层
 * 
 * @author zyg
 * 2020年2月8日 下午4:01:22
 */
public class CdsService {
	
	private Article artDao = new Article().dao();
	private Column colDao = new Column().dao();
	private Files filesDao = new Files().dao();

	/**
	 * 分页查询栏目文章
	 * @param page		页码
	 * @param limit		每页显示条数
	 * @param column	栏目id
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Page<Article> queryArtsPage(int page, int limit, int column){
		Kv kv = Kv.create();
		kv.put("siteId", SiteInfo.siteId);
		kv.put("column", column);
		return artDao.template("cds.queryArtsPage", kv).paginate(page, limit);
	}
	
	/**
	 * 根据id查询文章信息
	 * @param id
	 * @return
	 */
	public Article getArtById(int id){
		String sql = artDao.getSql("cds.getArtById");
		Article article = artDao.findFirst(sql, id);
		List<Files> files = getFilesByArt(article);
		article.put("fileList", files);
		return article;
	}
	
	/**
	 * 获取文章附件
	 * @param article
	 * @return
	 */
	public List<Files> getFilesByArt(Article article){
		String filesIds = article.getFiles();
		if(StrKit.isBlank(filesIds)){
			return null;
		}
		List<Files> fileList = new ArrayList<Files>();
		String[] ids = filesIds.split(",");
		int id = 0;
		for(int i=0,j=ids.length; i<j; i++){
			id = Integer.parseInt(ids[i]);
			fileList.add(filesDao.findById(id));
		}
		return fileList;
	}
	
	/**
	 * 点击文章
	 * <br/>点击文章需要做些隐藏操作，比如点击数更新
	 * @param id
	 */
	public void clickArt(int id){
		Article article = getArtById(id);
		int clickNum = article.getClickNum();
		clickNum++;
		article.setClickNum(clickNum);
		article.update();
	}
	
	/**
	 * 根据id查询栏目信息
	 * @param id
	 * @return
	 */
	public Column getColumnById(int id){
		return colDao.findById(id);
	}
	
	/**
	 * 根据栏目标识（英文名）获取栏目信息
	 * @param enName
	 * @return
	 */
	public Column getColumnByEnName(String enName){
		String sql = colDao.getSql("cds.getColumnByEnName");
		return colDao.findFirst(sql, enName, SiteInfo.siteId);
	}
	
	/**
	 * 根据子栏目获取父栏目信息
	 * @param column
	 * @return
	 */
	public Column getColumnByChild(Column column){
		int id = column.getParent();
		return getColumnById(id);
	}
}
