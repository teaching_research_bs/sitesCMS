package com.sites.cds.common;

import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Page;
import com.sites.common.model.Article;
import com.sites.common.model.Column;

/**
 * cds公用控制器
 * 
 * @author zyg
 * 2020年2月8日 下午3:58:18
 */
public class CdsController extends Controller {

	@Inject
	private CdsService srv;
	
	/**
	 * 首页
	 */
	public void index(){
		render("index.html");
	}
	
	/**
	 * 二级页面：普通栏目文章列表页面
	 */
	public void enterColArts(){
		int page = getParaToInt("page", 1);
		int limit = getParaToInt("limit", 8);
		String colEnName = getPara("col");
		Column column = srv.getColumnByEnName(colEnName);
		Column parentColumn = srv.getColumnByChild(column);
		Page<Article> artPage = srv.queryArtsPage(page, limit, column.getId());
		setAttr("colEnName", colEnName);
		setAttr("artPage", artPage);
		setAttr("column", column);
		setAttr("parentColumn", parentColumn);
		keepPara();
		render("pageColArts.html");
	}
	
	/**
	 * 二级页面：图片形式的栏目文章列表页面
	 */
	public void enterColPicArts(){
		int page = getParaToInt("page", 1);
		int limit = getParaToInt("limit", 8);
		String colEnName = getPara("col");
		Column column = srv.getColumnByEnName(colEnName);
		Column parentColumn = srv.getColumnByChild(column);
		Page<Article> artPage = srv.queryArtsPage(page, limit, column.getId());
		setAttr("colEnName", colEnName);
		setAttr("artPage", artPage);
		setAttr("column", column);
		setAttr("parentColumn", parentColumn);
		keepPara();
		render("pageColPicArts.html");
	}
	
	/**
	 * 二级页面：视频栏目文章列表页面
	 */
	public void enterColVideoArts(){
		int page = getParaToInt("page", 1);
		int limit = getParaToInt("limit", 8);
		String colEnName = getPara("col");
		Column column = srv.getColumnByEnName(colEnName);
		Column parentColumn = srv.getColumnByChild(column);
		Page<Article> artPage = srv.queryArtsPage(page, limit, column.getId());
		setAttr("colEnName", colEnName);
		setAttr("artPage", artPage);
		setAttr("column", column);
		setAttr("parentColumn", parentColumn);
		keepPara();
		render("pageColVideoArts.html");
	}
	
	/**
	 * 二级页面：特殊形式的栏目文章列表页面
	 */
	public void enterColSpecialArts(){
		int page = getParaToInt("page", 1);
		int limit = getParaToInt("limit", 8);
		String colEnName = getPara("col");
		Column column = srv.getColumnByEnName(colEnName);
		Column parentColumn = srv.getColumnByChild(column);
		Page<Article> artPage = srv.queryArtsPage(page, limit, column.getId());
		setAttr("colEnName", colEnName);
		setAttr("artPage", artPage);
		setAttr("column", column);
		setAttr("parentColumn", parentColumn);
		keepPara();
		render("pageColSpecialArts.html");
	}
	
	/**
	 * 二级页面：其他形式的栏目文章列表页面
	 */
	public void enterColOtherArts(){
		int page = getParaToInt("page", 1);
		int limit = getParaToInt("limit", 8);
		String colEnName = getPara("col");
		String viewName = getPara("view", "pageColOtherArts");
		Column column = srv.getColumnByEnName(colEnName);
		Column parentColumn = srv.getColumnByChild(column);
		Page<Article> artPage = srv.queryArtsPage(page, limit, column.getId());
		setAttr("artPage", artPage);
		setAttr("column", column);
		setAttr("parentColumn", parentColumn);
		keepPara();
		viewName = viewName + ".html";
		render(viewName);
	}
	
	/**
	 * 三级页面：普通文章页面
	 */
	public void viewArt(){
		int id = getParaToInt("id");
		Article article = srv.getArtById(id);
		srv.clickArt(id);
		Column column = srv.getColumnById(article.getColumn());
		Column parentColumn = srv.getColumnByChild(column);
		setAttr("article", article);
		setAttr("column", column);
		setAttr("parentColumn", parentColumn);
		setAttr("colEnName", column.getEnName());
		render("pageArt.html");
	}
	
	/**
	 * 三级页面：图片文章页面
	 */
	public void viewPicArt(){
		int id = getParaToInt("id");
		Article article = srv.getArtById(id);
		srv.clickArt(id);
		Column column = srv.getColumnById(article.getColumn());
		Column parentColumn = srv.getColumnByChild(column);
		setAttr("article", article);
		setAttr("column", column);
		setAttr("parentColumn", parentColumn);
		setAttr("colEnName", column.getEnName());
		render("pagePicArt.html");
	}
	
	/**
	 * 三级页面：视频文章页面
	 */
	public void viewVideoArt(){
		int id = getParaToInt("id");
		Article article = srv.getArtById(id);
		srv.clickArt(id);
		Column column = srv.getColumnById(article.getColumn());
		Column parentColumn = srv.getColumnByChild(column);
		setAttr("article", article);
		setAttr("column", column);
		setAttr("parentColumn", parentColumn);
		setAttr("colEnName", column.getEnName());
		render("pageVideoArt.html");
	}
	
	/**
	 * 三级页面：特殊文章页面
	 */
	public void viewSpecialArt(){
		int id = getParaToInt("id");
		Article article = srv.getArtById(id);
		srv.clickArt(id);
		Column column = srv.getColumnById(article.getColumn());
		Column parentColumn = srv.getColumnByChild(column);
		setAttr("article", article);
		setAttr("column", column);
		setAttr("parentColumn", parentColumn);
		setAttr("colEnName", column.getEnName());
		render("pageSpecialArt.html");
	}
	
	/**
	 * 三级页面：其他形式的文章页面
	 */
	public void viewOtherArt(){
		int id = getParaToInt("id");
		Article article = srv.getArtById(id);
		srv.clickArt(id);
		Column column = srv.getColumnById(article.getColumn());
		Column parentColumn = srv.getColumnByChild(column);
		setAttr("article", article);
		setAttr("column", column);
		setAttr("parentColumn", parentColumn);
		setAttr("colEnName", column.getEnName());
		
		String viewName = getPara("view", "pageOtherArt");
		viewName = viewName + ".html";
		render(viewName);
	}
	
}
