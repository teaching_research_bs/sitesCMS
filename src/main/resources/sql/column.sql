#sql("getParentColumns")
	select * from `column` t where t.parent = '0' and t.status = '1' and t.siteId = ? order by t.children,t.name
#end

#sql("getAllParentColumns")
	select * from `column` t where t.parent = '0' and t.siteId = ? order by t.children,t.name
#end

#sql("getByEnName")
	select * from `column` t where t.enName = ? and t.siteId = ? limit 1
#end

#sql("getAllColumns")
	select * from `column` t where t.siteId = ? order by t.parent,t.name
#end

#sql("getChildrenColumns")
	select * from `column` t where t.status = '1' and t.parent = ? and t.siteId = ? order by t.name
#end

#sql("getAllChildrenColumns")
	select * from `column` t where t.parent = ? and t.siteId = ? order by t.name
#end

#sql("delColumnSql")
	update `column` t set t.`status` = ? where t.id = ? and t.siteId = ?
#end