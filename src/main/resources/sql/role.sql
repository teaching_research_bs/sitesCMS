#sql("getByName")
	select * from role t where t.name = ? and t.siteId = ? limit 1
#end

#sql("queryAllRoles")
	select * from role t where t.siteId = ? order by t.updateTime desc,t.createTime desc
#end

#sql("getRolePerm")
	select * from rolepermission t where t.roleId = ?
#end

#sql("addPerm")
	insert into rolepermission(roleId,permissionId) values (?, ?)
#end

#sql("delPerm")
	delete from rolepermission where roleId = ? and permissionId = ?
#end

#sql("getSiteRolePerm")
	SELECT
		r.id roleId,
		r.name roleName,
		p.id permId,
		p.actionKey
	FROM
		role r
	LEFT JOIN rolepermission rp ON r.id = rp.roleId
	LEFT JOIN permission p ON rp.permissionId = p.id
	WHERE
		r.siteId = '0'
	AND p.siteId = '0'
#end

#sql("addRolePerm")
 insert into rolepermission(roleId, permissionId) values (?, ?)
#end