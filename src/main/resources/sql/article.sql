#sql("queryArtsPage")
	SELECT
		t.*,
		c.`name` AS columnName,
		a1.nickName AS createAccountName,
		a2.nickName AS updateAccountName
	FROM
		article t
	LEFT JOIN `column` c ON t.`column` = c.id
	LEFT JOIN account a1 ON t.createAccount = a1.id
	LEFT JOIN account a2 ON t.updateAccount = a2.id
	WHERE
		t.siteId = #para(siteId)
	#if(title)
	AND t.title LIKE #para(title)
	#end
	#if(column>0)
	AND t.`column` = #para(column)
	#end
	#if(type!='0')
	AND t.type = #para(type)
	#end
	AND t.`status` = #para(status)
	ORDER BY
		t.updateTime DESC,
		t.createTime DESC
#end

#sql("delArtByCol")
	update article t set t.`status` = ?, t.updateAccount = ? where t.`column` = ? and t.siteId = ?
#end

#sql("queryArticleNums")
	SELECT
		t.type,
		count(t.id) num
	FROM
		article t
	WHERE
		t.`status` = '1'
	AND t.siteId = ?
	GROUP BY
		t.type
#end

#sql("countArtClickNums")
	SELECT
		ifnull(sum(t.clickNum),0)
	FROM
		article t
	WHERE
		t.siteId = ?
	AND t.`status` = '1'
#end